var util =  require('util');
var nd = require('node-dir');
var path = require('path');
var fs = require('fs');
var shell = require('shelljs');
var exec = require("child_process").exec

var List = function() {};

List.prototype.getAll = function(callback){
	var environment = "test";
	var version = "";
	var table = {};
	
	var listConfigs = function() {
		table = {};
		basePath = process.cwd();
		files = nd.readFiles(basePath, {
			match: /config.json/,
			exclude: /^\./,
			excludeDir: /node_modules/
			}, function(err, content, filename, next) {
			if (err) {
				throw err;
			}
			return next();
			}, function(err, files) {
				var file, newFiles, requestedClients, _i, _len;
			if (err) {
				throw err;
			}
			for (_i = 0, _len = files.length; _i < _len; _i++) {
				file = files[_i];
				brand = findClientRoot(file.slice(0, -12));
				if (brand != null) {
					console.log(brand["brand"]["path"])
					brandString = brand["brand"]["path"];
					table.brandString = {"name": brandString, "path": file.slice(0, -11).slice(process.cwd().length, -1), "version": brand["version"]};
				} else {
					continue;
				}
			}
		});
		return table;
	};

	var findClientRoot = function(basePath) {
		var dir, envRoot, err, oldPath, path, qelpjsBin, result, _i, _len;
		result = {};
		basePath = basePath || process.cwd();
		envRoot = basePath + "/public/build/" + environment;
		result["envRoot"] = envRoot;
		result["basePath"] = basePath;
		qelpjsBin = null;
		path = process.env.PATH.split(":");
		for (_i = 0, _len = path.length; _i < _len; _i++) {
			dir = path[_i];
			if (fs.existsSync(dir + "/qelpjs")) {
				qelpjsBin = dir + "/qelpjs";
				break;
			}
		}
		if ((qelpjsBin != null) && false === true) {
			console.log("Auto-building client at location: " + basePath, "N");
			oldPath = process.cwd();
			try {
				process.chdir(basePath);
				console.log("New directory: " + process.cwd(), "N");
			} catch (_error) {
				err = _error;
				console.log("chdir: " + err);
			}
			shell.exec(qelpjsBin + " build", function(error, stdout, stderr) {
				if (stderr != null) {
					console.log(stderr.toString(), "W");
				}
				console.log("stdout: " + stdout, "N");
				if (error != null) {
					return console.log("exec error: " + error, "W");
				}
			});
			try {
				process.chdir(oldPath);
				console.log("New directory: " + process.cwd(), "N");
			} catch (_error) {
				err = _error;
				console.log("chdir: " + err);
			}
		}
		if (!fs.existsSync(envRoot)) {
			return;
		}
		// console.log("Build root exists so we can do stuffs", "N");
		if (typeof result["brand"] === void 0) {
			return false;
		}
		result = findConfig(basePath, result);
		return result;
	};

	var findConfig = function(basePath, result) {
		var file;
		// console.log("Searching for config file at " + basePath, "N");
		if (fs.existsSync(basePath + "/config.json")) {
			file = fs.readFileSync(basePath + "/config.json", "utf-8");
			if (file != null) {
				result["brand"] = JSON.parse(file);
				// console.log("Config brand is " + result["brand"]["name"], "N");
				result = findClientVersion(result);
				if (result["brand"] === null) {
					console.log("Empty or unreadable config file");
					return;
				}
			}
		}
		return result;
	};

	var findClientVersion = function(brand) {
		version = shell.exec("git describe --match " + brand["brand"]["path"].toLowerCase() + "-* --tags --abbrev=0", {silent: true}).output;
		var match = version.match("fatal")
		if (match == null) {
			brand.version = version.match('[a-z].+v[0-9]{1,}.[0-9]{1,}.[0-9]{1,}').toString();
		} else {
			brand.version = "No tags found";
		}
		return brand;
	};

	if (callback) { callback(table) };

	return {
		listConfigs: listConfigs
	}
}();

exports.index = List;

