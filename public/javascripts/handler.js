function setup() {
	$("button.deploy").on("click", function(event) {
		event.preventDefault();
		// console.log("deploy", $(this).parents('div.client-row').data("path").toString());
		client = $(this).parents('div.client-row').data("path").toString();
		$.ajax({
			type: "GET",
			data: encodeURI(client),
			url: "/deploy",
			success: function(data) {
				console.log("success");
			},
			error: function(data) {
				console.log("error");
			}
		});
	});
	$("button.server-start").on("click", function(event) {
		event.preventDefault();
		// console.log("start-server", $(this).parents('div.client-row').data("path").toString());
		client = $(this).parents('div.client-row').data("path").toString();
		$.ajax({
			type: "GET",
			data: encodeURI(client),
			url: "/startserver",
			loading: function() {
				console.log("loading");
			},
			success: function(data) {
				console.log("success");
			},
			error: function(data) {
				console.log("error");
			}
		});
	});

	$('.client-info').on("click", function(event) {
		event.preventDefault();
		clientPath = $(this).parents('.client-row')[0].dataset.path; 
		window.location(clientPath);
	});
};

$(document).ready(
	setup()
);
