var exec = require("child_process").exec
var fs = require('fs');
var path = require('path');
var shell = require('shelljs');
var util =  require('util');

Deploy = function() {
	function Deploy() {};

	var findQelpJSBin = function(basePath) {
		console.log(basePath);
		var qelpjsBin = exec('which qelpjs', function(err, data) {
			if ((qelpjsBin != null)) {
				oldPath = process.cwd();
				try {
					process.chdir(basePath);
				} catch (_error) {
					err.push(_error);
				}
				shell.exec(qelpjsBin + " build", function(error, stdout, stderr) {
					if (stderr != null) {
						err.push("QelpJS failed to work!")
					}
					if (error != null) {
						return console.log("exec error: " + error, "W");
					}
				});
				try {
					process.chdir(oldPath);
				} catch (_error) {
					err.push(_error);
				}
			}
		})();
	}

	Deploy.prototype.startServer = function(basePath, callback) {
		this.basePath = basePath || process.cwd();
		this.err = [];

		var that = this;

		console.log(this.findQelpJSBin);

		if (callback) {
			callback(err, data);
		};
	}

	Deploy.prototype.deployClient = function(basePath, callback) {
		this.basePath = basePath || process.cwd();
		this.err = [];

		var that = this;

		console.log(this.findQelpJSBin);

		var findClientRoot = function(basePath) {
			var result = {};
			var basePath = that.basePath;
			envRoot = basePath + "/public/build/"+ environment;
			result["envRoot"] = envRoot;
			result["basePath"] = basePath;
			var qelpJS = findQelpJSBin(basePath);

			if (fs.existsSync(envRoot) !== true) {
				err.push("Client was not built for "+environment+", and qelpjs was not found in $PATH. Run npm -g install inside the QelpJS-Node project first");
				return;
			}

			result = findConfig(basePath, result);

			if (typeof(result["brand"]) === undefined) {
				return false;
			}
			return result;
		}

		if (callback) {
			callback(err, data);
		};

	}
	return Deploy;
}()

module.exports = Deploy;