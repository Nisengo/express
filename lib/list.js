var exec = require("child_process").exec
var fs = require('fs');
var nd = require('node-dir');
var path = require('path');
var shell = require('shelljs');
var util =  require('util');

List = function() {
	function List() {}

	List.prototype.getAll = function(callback) {
		var basePath = process.cwd();
		var environment = "test";
		var version = "";
		var table = [];
		var err = {};

		var findClientRoot = function(basePath) {
			var dir, envRoot, err, oldPath, path, qelpjsBin, result, _i, _len;
			result = {};
			basePath = basePath || process.cwd();
			envRoot = basePath + "/public/build/" + environment;
			result["envRoot"] = envRoot;
			result["basePath"] = basePath;
			qelpjsBin = null;
			path = process.env.PATH.split(":");
			for (_i = 0, _len = path.length; _i < _len; _i++) {
				dir = path[_i];
				if (fs.existsSync(dir + "/qelpjs")) {
					qelpjsBin = dir + "/qelpjs";
					break;
				}
			}
			if (!fs.existsSync(envRoot)) {
				return;
			}
			// console.log("Build root exists so we can do stuffs", "N");
			if (typeof result["brand"] === void 0) {
				return false;
			}
			result = findConfig(basePath, result);
			return result;
		};

		var findConfig = function(basePath, result) {
			var file;
			// console.log("Searching for config file at " + basePath, "N");
			if (fs.existsSync(basePath + "/config.json")) {
				file = fs.readFileSync(basePath + "/config.json", "utf-8");
				if (file != null) {
					result["brand"] = JSON.parse(file);
					// console.log("Config brand is " + result["brand"]["name"], "N");
					result = findClientVersion(result);
					if (result["brand"] === null) {
						return;
					}
				}
			}
			return result;
		};

		var findClientVersion = function(brand) {
			version = shell.exec("git describe --match " + brand["brand"]["path"].toLowerCase() + "-* --tags --abbrev=0", {silent: true}).output;
			var match = version.match("fatal")
			if (match == null) {
				brand.version = version.match('[a-z].+v[0-9]{1,}.[0-9]{1,}.[0-9]{1,}').toString();
			} else {
				brand.version = "No tags found";
			}
			return brand;
		};

		nd.readFiles(basePath, {
			match: /config.json/,
			exclude: /^\./,
			excludeDir: /node_modules/
			}, function(err, content, filename, next) {
			if (err) {
				throw err;
			}
			return next();
			}, function(err, files) {
				var file, newFiles, requestedClients, _i, _len;
			if (err) {
				throw err;
			}
			for (_i = 0, _len = files.length; _i < _len; _i++) {
				file = files[_i];
				brand = findClientRoot(file.slice(0, -12));
				if (brand != null) {
					brandString = brand["brand"]["path"];
					var major = file.match('v[0-9]{1,2}\.[0-9]{1,2}');
					if (major && major[0].match("\.")) {
						major = major[0].replace('\.', '-');
					} else {
						major = "none";
					}
					var tagged = true;
					if (brand["version"] === "No tags found") {
						tagged = false;
					};
					table.push({
						"name": brandString,
						"path": file.slice(0, -11).slice(process.cwd().length, -1),
						"version": brand["version"],
						"major": major,
						"tagged": tagged
					});
				} else {
					continue;
				}
			}
			if (callback) {
				callback(err, table)
			};
		});

	}
	return List;
}()

module.exports = List;